<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="es">
<head>
<?php
	require_once("head.php");
	show_head("Inicio");
?>
</head>

<body>
<?php
	require_once("menu.php");
	show_menu("inicio","");
?>
		<div class="container">
			<div class="jumbotron">
				<h1>AdminParroq</h1>
				<p>Sistema de Gestión Parroquial</p>
			</div>
		</div>
<?php
	require("footer.php")
?>
</body>

</html>