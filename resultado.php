<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Resultados");
?>
    <script>
	$(document).ready(function() 
	    { 
	        $("#tabla_resultado").tablesorter(); 
	    } 
	); 
	</script>

</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$busqueda = $_GET['busqueda'];
	if ($busqueda == '')
	{
		echo "<script language='javascript'>window.location='buscar.php'</script>";
		exit;
	}
?>
<?php
	$busqueda = str_replace(" ","%",$busqueda);
	$busqueda = "%".$busqueda."%";
?>
<?php
	require('conexion.php')
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<table class="table tablesorter" id="tabla_resultado">
					<thead>
						<tr>
							<th>Tipo</th>
							<th>Acta</th>
							<th>Nombre</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
<?php
	$sql = "SELECT id_bautismo, acta, nombre, apellido_paterno, apellido_materno FROM bautismo WHERE (concat_ws(' ', nombre, apellido_paterno, apellido_materno) LIKE '$busqueda' OR acta LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>BAUTISMO</td>";
			echo "<td>".$row['acta']."</td>";
			echo "<td>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_bautismo.php?id_bautismo=".$row['id_bautismo']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_bautismo.php?id_bautismo=".$row['id_bautismo']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
<?php
	$sql = "SELECT id_confirmacion, acta, nombre, apellido_paterno, apellido_materno FROM confirmacion WHERE (concat_ws(' ', nombre, apellido_paterno, apellido_materno) LIKE '$busqueda' OR acta LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>CONFIRMACION</td>";
			echo "<td>".$row['acta']."</td>";
			echo "<td>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_confirmacion.php?id_confirmacion=".$row['id_confirmacion']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_confirmacion.php?id_confirmacion=".$row['id_confirmacion']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
<?php
	$sql = "SELECT id_eucaristia, acta, nombre, apellido_paterno, apellido_materno FROM eucaristia WHERE (concat_ws(' ', nombre, apellido_paterno, apellido_materno) LIKE '$busqueda' OR acta LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>EUCARISTIA</td>";
			echo "<td>".$row['acta']."</td>";
			echo "<td>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_eucaristia.php?id_eucaristia=".$row['id_eucaristia']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_eucaristia.php?id_eucaristia=".$row['id_eucaristia']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
<?php
	$sql = "SELECT id_matrimonio, acta, persona1_nombre, persona1_apellido_paterno, persona1_apellido_materno FROM matrimonio WHERE (concat_ws(' ', persona1_nombre, persona1_apellido_paterno, persona1_apellido_materno) LIKE '$busqueda' OR acta LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>MATRIMONIO</td>";
			echo "<td>".$row['acta']."</td>";
			echo "<td>".$row['persona1_nombre']." ".$row['persona1_apellido_paterno']." ".$row['persona1_apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_matrimonio.php?id_matrimonio=".$row['id_matrimonio']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_matrimonio.php?id_matrimonio=".$row['id_matrimonio']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
<?php
	$sql = "SELECT id_matrimonio, acta, persona2_nombre, persona2_apellido_paterno, persona2_apellido_materno FROM matrimonio WHERE (concat_ws(' ', persona2_nombre, persona2_apellido_paterno, persona2_apellido_materno) LIKE '$busqueda' OR acta LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>MATRIMONIO</td>";
			echo "<td>".$row['acta']."</td>";
			echo "<td>".$row['persona2_nombre']." ".$row['persona2_apellido_paterno']." ".$row['persona2_apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_matrimonio.php?id_matrimonio=".$row['id_matrimonio']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_matrimonio.php?id_matrimonio=".$row['id_matrimonio']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
<?php
	$sql = "SELECT id_ordenacion, nombre, apellido_paterno, apellido_materno FROM ordenacion WHERE (concat_ws(' ', nombre, apellido_paterno, apellido_materno) LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>ORDENACION</td>";
			echo "<td>"."</td>";
			echo "<td>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_ordenacion.php?id_ordenacion=".$row['id_ordenacion']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_ordenacion.php?id_ordenacion=".$row['id_ordenacion']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
<?php
	$sql = "SELECT id_profesion, nombre, apellido_paterno, apellido_materno FROM profesion WHERE (concat_ws(' ', nombre, apellido_paterno, apellido_materno) LIKE '$busqueda') AND id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		}
		$div_date = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
			echo "<tr>";
			echo "<td>PROFESION</td>";
			echo "<td>"."</td>";
			echo "<td>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</td>";
			echo "<td>";
			echo "<div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_profesion.php?id_profesion=".$row['id_profesion']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_profesion.php?id_profesion=".$row['id_profesion']."'>Modificar</a>";
			}
			echo "<a type='button' class='btn btn-primary'>Imprimir</a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "\n";
		}
?>
			 		</tbody>
			 	</table>
			 </div>
<?php
	require("footer.php")
?>
</body>

</html>