<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Bautismo");
?>
    <script>
    	function DatePicker(){

    		$('#date_registro_civil_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    		$('#date_nacimiento_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    		$('#date_bautismo_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}
    </script>

</head>
<body onload="DatePicker()">
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_bautismo = $_GET["id_bautismo"];
	$ministro_nombre;

	require('conexion.php');
	
	$ministro_nombre = "";
 
	$sql = "SELECT * FROM bautismo WHERE id_bautismo = $id_bautismo AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	if($row['bautismo_ministro'] != null){
		$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[bautismo_ministro] AND id_parroquia = $_SESSION[id_parroquia]";

		$result = $conexion->query($sql);
		$row2 = $result->fetch_array(MYSQLI_ASSOC);
		$ministro_nombre = $row2['nombre']." ".$row2['apellido_materno']." ".$row2['apellido_materno'];
	}

	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[bautismo_parroco] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row3 = $result->fetch_array(MYSQLI_ASSOC);

	$sql = "SELECT templo FROM iglesia WHERE id_iglesia = $row[id_iglesia] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row4 = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['bautismo_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$bautismo_fecha = implode("/", $array_date);

	$div_date = explode("-",$row['nacimiento_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$nacimiento_fecha = implode("/", $array_date);

	$div_date = explode("-",$row['registro_civil_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$registro_civil_fecha = implode("/", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(1);
?>
			<div class="container">
				<div class="col-lg-2"></div>
			</div>
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Bautismo</h1></div>
			<div class="container">
			</div>
			<form action="set_update.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="1">
				<input type="hidden" name="id_bautismo" value="<?php echo $_GET["id_bautismo"];?>">
				<div class="container">
					<div class="col-lg-4">
						<div>
							<h4>Datos Personales</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" value="<?php echo $row['nombre'];?>" type="text" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" 
								value="<?php echo $row['apellido_paterno'];?>" type="text" name="apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" value="<?php echo $row['apellido_materno'];?>" type="text" name="apellido_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Nacimiento: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" value="<?php echo $row['nacimiento_lugar'];?>" type="text" name="nacimiento_lugar" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Nacimiento: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" value="<?php echo $nacimiento_fecha;?>" id="date_nacimiento_fecha" name="nacimiento_fecha">
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<label><input type="radio" name="genero" value="M" <?php if($row['genero'] == "M"){ echo "checked"; }?>>Masculino</label>
								<label><input type="radio" name="genero" value="F" <?php if($row['genero'] == "F"){ echo "checked"; }?>>Femenino</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['padre_nombre'];?>" name="padre_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['madre_nombre'];?>" name="madre_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuelo Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['abuelo_paterno'];?>"" name="abuelo_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuela Paterna: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['abuela_paterna'];?>" name="abuela_paterna" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuelo Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['abuelo_materno'];?>" name="abuelo_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuela Materna: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['abuela_materna'];?>" name="abuela_materna" required>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos de Bautismo</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4" for="sel1">Iglesia:</label>
							<div class="col-lg-8">
								<select class="form-control" name="id_iglesia" id="sel1">
<?php
	$sql = "SELECT id_iglesia, templo FROM iglesia where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) {
		while($row5 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row5['id_iglesia'] == $row['id_iglesia']){
			echo "<option value='".$row5['id_iglesia']."' selected>".$row5['templo']."</option>";
			}else{
			echo "<option value='".$row5['id_iglesia']."'>".$row5['templo']."</option>";
			}
		} 
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" value="<?php echo $row['acta'];?>" name="acta" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Libro: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" value="<?php echo $row['libro'];?>" name="libro" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Bautismo: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_bautismo_fecha" name="bautismo_fecha" 
								value="<?php echo $bautismo_fecha;?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Ministro de Bautismo: </label>
							<div class="col-lg-8">
								<select class="form-control" name="bautismo_ministro" id="sel1">
									<option value=''>NINGUNO</option>
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row5 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row5['id_principal'] == $row['bautismo_ministro']){
			echo "<option value='".$row5['id_principal']."' selected>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			} else {
				echo "<option value='".$row5['id_principal']."'>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			}
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Párroco de Bautismo: </label>
							<div class="col-lg-8">
								<select class="form-control" name="bautismo_parroco" id="sel1">
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row5 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row5['id_principal'] == $row['bautismo_parroco']){
			echo "<option value='".$row5['id_principal']."' selected>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			} else {
				echo "<option value='".$row5['id_principal']."'>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			}
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Padrino de Bautismo: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['bautismo_padrino'];?>" name="bautismo_padrino" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Madrina de Bautismo: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['bautismo_madrina'];?>" name="bautismo_madrina" required>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos de Registro Civil</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Registro: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['registro_civil_lugar'];?>" name="registro_civil_lugar" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Registro: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_registro_civil_fecha" value="<?php echo $registro_civil_fecha;?>" name="registro_civil_fecha" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta de Registro: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['registro_civil_acta'];?>" name="registro_civil_acta" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>