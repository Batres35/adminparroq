<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Bautismo");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_bautismo = $_GET["id_bautismo"];
	$ministro_nombre;

	require('conexion.php');
	
	$ministro_nombre = "";
 
	$sql = "SELECT * FROM bautismo WHERE id_bautismo = $id_bautismo AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	if($row['bautismo_ministro'] != null){
		$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[bautismo_ministro] AND id_parroquia = $_SESSION[id_parroquia]";

		$result = $conexion->query($sql);
		$row2 = $result->fetch_array(MYSQLI_ASSOC);
		$ministro_nombre = $row2['nombre']." ".$row2['apellido_materno']." ".$row2['apellido_materno'];
	}

	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[bautismo_parroco] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row3 = $result->fetch_array(MYSQLI_ASSOC);

	$sql = "SELECT templo FROM iglesia WHERE id_iglesia = $row[id_iglesia] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row4 = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['bautismo_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$bautismo_fecha = implode("-", $array_date);

	$div_date = explode("-",$row['nacimiento_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$nacimiento_fecha = implode("-", $array_date);

	$div_date = explode("-",$row['registro_civil_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$registro_civil_fecha = implode("-", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Ver Bautismo</h1></div>
			</div>
			<form method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="1">
				<div class="container">
					<div class="col-lg-4">
						<div>
							<h4>Datos Personales</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['apellido_paterno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Nacimiento: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['nacimiento_lugar'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Nacimiento: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $nacimiento_fecha;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['genero'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['padre_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['madre_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuelo Paterno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['abuelo_paterno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuela Paterna: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['abuela_paterna'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuelo Materno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['abuelo_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Abuela Materna: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['abuela_materna'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos de Bautismo</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4" for="sel1">Iglesia:</label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row4['templo'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['acta'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Libro: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['libro'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Bautismo: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $bautismo_fecha;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Ministro de Bautismo: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $ministro_nombre;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Párroco de Bautismo: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row3['nombre']." ".$row3['apellido_materno']." ".$row3['apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Padrino de Bautismo: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['bautismo_padrino'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Madrina de Bautismo: </label>
							<div class="col-lg-8">
							<p class="form-control-static"><?php echo $row['bautismo_madrina'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos de Registro Civil</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Registro: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['registro_civil_lugar'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Registro: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $registro_civil_fecha;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta de Registro: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['registro_civil_acta'];?></p>
							</div>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>