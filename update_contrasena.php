<?php
session_start();
require_once('check_loggedin.php');
check_loggedin(0);
?>

<?php
require('log/log.php');
require_once('conexion.php');

$id_usuario = htmlspecialchars($_SESSION["id_usuario"], ENT_QUOTES, 'UTF-8');

$contrasena = $_POST['contrasena'];
$nueva_contrasena = $_POST['nueva_contrasena'];
$repetir_contrasena = $_POST['repetir_contrasena'];

$sql = "SELECT contrasena FROM usuario WHERE id_usuario = '$id_usuario'";
$result = $conexion->query($sql);
$row = $result->fetch_array(MYSQLI_ASSOC);

if (password_verify($contrasena, $row['contrasena'])) {
	if ($nueva_contrasena != $repetir_contrasena)
	{
		echo "Las contraseñas no coinciden";
	} else {
		if ($nueva_contrasena != "" && $repetir_contrasena != ""){

			$hash = password_hash($nueva_contrasena, PASSWORD_BCRYPT);

			$query = "UPDATE usuario SET 
			contrasena='$hash'
			WHERE 
			id_usuario = $id_usuario
			";

		} else {
			echo "Error al cambiar contraseña.";
		}

		if ($conexion->query($query) === TRUE) {
			echo "<h2>Contraseña Cambiada Exitosamente</h2>";
			echo "<script language='javascript'>window.location='logout.php'</script>";
			write_log("log/","EL USUARIO ".$_SESSION['username']." CON ID ".$_SESSION['id_usuario']." CAMBIO SU CONTRASEÑA.");
		} else {
			echo "Error modificar contrasena." . $query . "<br>" . $conexion->error; 
		}
	}
} else {
	echo "Contraseña Incorrecta, vuelva a intentarlo.";
}
mysqli_close($conexion);
?>