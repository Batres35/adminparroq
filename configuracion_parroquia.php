<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Configuración de la Parroquia");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require_once("menu.php");
	show_menu("parroquia","configuracion_parroquia");
?>
<?php
	check_loggedin(1);
?>
<?php
	require_once('conexion.php');

	$id_parroquia = $_SESSION['id_parroquia'];
 
	$sql = "SELECT * FROM parroquia WHERE id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['vigencia_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$vigencia_fecha = implode("/", $array_date);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Configuración de la Parroquia</h1></div>
			</div>
			<div class="container">
				<form action="update_parroquia.php" method="post" class="form-horizontal">
				<div class="container">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" value="<?php echo $row['nombre']?>" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Estado: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" value="<?php echo $row['estado']?>" name="estado" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Municipio: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" value="<?php echo $row['municipio']?>" name="municipio" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Localidad: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" value="<?php echo $row['localidad']?>" name="localidad" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Direccion: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" value="<?php echo $row['direccion']?>" name="direccion" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Telefono: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" value="<?php echo $row['telefono']?>" name="telefono" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-10">
								<input class="form-control" type="text" value="<?php echo $row['correo_electronico']?>" name="correo_electronico" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Parroco: </label>
							<div class="col-lg-10">
								<select class="form-control" name="parroco" id="sel1">
									<option value=''>NINGUNO</option>
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row2 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row2['id_principal'] == $row['parroco']){
			echo "<option value='".$row2['id_principal']."' selected>".$row2['nombre']." ".$row2['apellido_paterno']." ".$row2['apellido_materno']."</option>";
			} else {
				echo "<option value='".$row2['id_principal']."'>".$row2['nombre']." ".$row2['apellido_paterno']." ".$row2['apellido_materno']."</option>";
			}
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Fecha de Vigencia: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_vigencia_fecha" value="<?php echo $vigencia_fecha ?>" id="date_vigencia_fecha" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Guardar Configuración</button>
						</div>
					</div>
				</div>
				</form>
			</div>
<?php
	require("footer.php")
?>
</body>
</html>