<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Alta Eucaristía");
?>
    
    <script>
    	function DatePicker(){
    		$('#date_eucaristia_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    		$('#date_bautismo_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}
    </script>

</head>
<body onload="DatePicker()">
<?php
	require_once('check_loggedin.php');
?>
<?php
	require('conexion.php');
 
	$sql = "SELECT id_iglesia, templo FROM iglesia where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);
?>
<?php
	require_once("menu.php");
	show_menu("alta","alta_eucaristia");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class='alert alert-warning col-lg-8'>
					<strong>Por favor verifica que la información sea correcta antes de continuar.</strong>
					<p>No es posible modificar la información posteriormente.</p>
				</div>
			</div>
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Eucaristía</h1></div>
			<div class="container">
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="3">
				<div class="container">
					<div class="col-lg-2"></div>
					<div class="col-lg-4">
						<div>
							<h4>Datos Personales</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="apellido_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<label><input type="radio" name="genero" value="M">Masculino</label>
								<label><input type="radio" name="genero" value="F">Femenino</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="padre_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="madre_nombre" required>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos de Eucaristía</h4>	
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4" for="sel1">Iglesia:</label>
							<div class="col-lg-8">
								<select class="form-control" name="id_iglesia" id="sel1">
<?php
	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			echo "<option value='".$row['id_iglesia']."''>".$row['templo']."</option>";
		} 
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" name="acta" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Libro: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" name="libro">
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Eucaristía: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_eucaristia_fecha" name="eucaristia_fecha" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Ministro de Eucaristía: </label>
							<div class="col-lg-8">
								<select class="form-control" name="eucaristia_ministro" id="sel1">
									<option value=''>NINGUNO</option>
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			echo "<option value='".$row['id_principal']."'>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</option>";
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Párroco de Eucaristía: </label>
							<div class="col-lg-8">
								<select class="form-control" name="eucaristia_parroco" id="sel1">
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			echo "<option value='".$row['id_principal']."'>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</option>";
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Padrino de Eucaristía: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="eucaristia_padrino">
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Madrina de Eucaristía: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="eucaristia_madrina">
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Bautismo: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_bautismo_fecha" name="bautismo_fecha" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Registrar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>