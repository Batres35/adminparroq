<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Eucaristía");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_eucaristia = $_GET["id_eucaristia"];
	$ministro_nombre = "";
 
	require('conexion.php');
 
	$sql = "SELECT * FROM eucaristia WHERE id_eucaristia = $id_eucaristia AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	if($result != ""){
		$row = $result->fetch_array(MYSQLI_ASSOC);
	}

	if(isset($row['eucaristia_ministro'])){
		$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[eucaristia_ministro] AND id_parroquia = $_SESSION[id_parroquia]";

		$result = $conexion->query($sql);
		$row2 = $result->fetch_array(MYSQLI_ASSOC);
		$ministro_nombre = $row2['nombre']." ".$row2['apellido_materno']." ".$row2['apellido_materno'];
	}

	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[eucaristia_parroco] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	if($result != ""){
		$row3 = $result->fetch_array(MYSQLI_ASSOC);
	}

	$sql = "SELECT templo FROM iglesia WHERE id_iglesia = $row[id_iglesia] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	if($result != ""){
		$row4 = $result->fetch_array(MYSQLI_ASSOC);
	}
	
	if(isset($row['eucaristia_fecha']) && isset($row['bautismo_fecha'])){
		$div_date = array();

		$div_date = explode("-",$row['eucaristia_fecha']);
		$array_date = array($div_date[2], $div_date[1], $div_date[0]);
		$eucaristia_fecha = implode("-", $array_date);

		$div_date = explode("-",$row['bautismo_fecha']);
		$array_date = array($div_date[2], $div_date[1], $div_date[0]);
		$bautismo_fecha = implode("-", $array_date);
	}
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Ver Eucaristía</h1></div>
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="3">
				<div class="container">
					<div class="col-lg-2"></div>
					<div class="col-lg-4">
						<div>
							<h4>Datos Personales</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['apellido_paterno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<p class="form-control-static"><?php if($row['genero'] != null) { echo $row['genero']; }?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['padre_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['madre_nombre'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos de Eucaristía</h4>	
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4" for="sel1">Iglesia:</label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row4['templo'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['acta'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Libro: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['libro'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Eucaristía: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $eucaristia_fecha;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Ministro de Eucaristía: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $ministro_nombre;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Párroco de Eucaristía: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row3['nombre']." ".$row3['apellido_materno']." ".$row3['apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Padrino de Eucaristía: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php if($row['eucaristia_padrino'] != null) { echo $row['eucaristia_padrino']; }?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Madrina de Eucaristía: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php if($row['eucaristia_madrina'] != null) { echo $row['eucaristia_madrina']; }?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Bautismo: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $bautismo_fecha;?></p>
							</div>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>