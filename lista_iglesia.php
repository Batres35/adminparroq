<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Lista de Iglesias");
?>
	<script>
	$(document).ready(function() 
	    { 
	        $("#tabla_iglesia").tablesorter(); 
	    } 
	); 
	</script>

</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require('conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("parroquia","lista_iglesia");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
			</div>
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Lista de Iglesias</h1></div>
			<div class="container">
				<table class="table tablesorter" id="tabla_iglesia">
				    <thead>
				      	<tr>
					        <th>Templo</th>
					        <th>Parroquia</th>
					        <th>Arquidiócesis</th>
					        <th>Opciones</th>
				      	</tr>
				    </thead>
				    <tbody>
<?php
	$sql = "SELECT * FROM iglesia where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);
	//ver_iglesia?id_iglesia=3
	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			echo "<tr>";
			echo "<td>".$row['templo']."</td>";
			echo "<td>".$row['parroquia']."</td>";
			echo "<td>".$row['arquidiocesis']."</td>";
			echo "<td><div class='btn-group'><a type='button' class='btn btn-primary' href='ver_iglesia.php?id_iglesia=".$row['id_iglesia']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_iglesia.php?id_iglesia=".$row['id_iglesia']."'>Modificar</a>";
			}
			echo "</div></td>";
			echo "</tr>";
		}
	}
?>
				      
				    </tbody>
			  	</table>
			</div>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>