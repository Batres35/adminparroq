<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Profesión");
?>
    <script>
    	function DatePicker(){
    		$('#date_profesion_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}
    </script>
</head>
<body onload="DatePicker()">
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_profesion = $_GET["id_profesion"];

	require('conexion.php');
	$sql = "SELECT * FROM profesion WHERE id_profesion = $id_profesion AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['profesion_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$profesion_fecha = implode("/", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(1);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Profesión</h1></div>
			</div>
			<form action="set_update.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="5">
				<input type="hidden" name="id_profesion" value="<?php echo $_GET["id_profesion"];?>">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['nombre'];?>" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['apellido_paterno'];?>" name="apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['apellido_materno'];?>" name="apellido_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Profesión: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['profesion_lugar'];?>" name="profesion_lugar" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Profesión: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_profesion_fecha" value="<?php echo $profesion_fecha;?>" name="profesion_fecha" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>