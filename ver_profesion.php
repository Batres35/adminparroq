<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Profesión");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_profesion = $_GET["id_profesion"];

	require('conexion.php');
	$sql = "SELECT * FROM profesion WHERE id_profesion = $id_profesion AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['profesion_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$profesion_fecha = implode("-", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Ver Profesión</h1></div>
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="5">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['apellido_paterno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Profesión: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['profesion_lugar'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Profesión: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $profesion_fecha;?></p>
							</div>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>