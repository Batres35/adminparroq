<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Cambiar Contraseña");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require_once("menu.php");
	show_menu("cuenta","");
?>
<?php
	check_loggedin(0);
?>
	<div class="container">
		<div class="col-lg-2"></div>
		<div class="col-lg-8"><h1>Cambiar Contraseña</h1></div>
	</div>
	<div class="container">
		<form action="update_contrasena.php" method="post" class="form-horizontal">
			<input type="hidden" name="id_usuario" value="<?php echo $_SESSION["id_usuario"];?>">
			<div class="col-lg-4"></div>
			<div class="col-lg-6">
				<div class="form-group row">
					<label class="control-label col-lg-2">Contraseña Anterior: </label>
					<div class="col-lg-6">
						<input class="form-control uppercase" type="password" name="contrasena" maxlength="12" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-lg-2">Nueva Contraseña: </label>
					<div class="col-lg-6">
						<input class="form-control uppercase" type="password" name="nueva_contrasena" maxlength="12" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-lg-2">Repetir Contraseña: </label>
					<div class="col-lg-6">
						<input class="form-control uppercase" type="password" name="repetir_contrasena" maxlength="12" required>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="col-lg-4"></div>
				<div class="col-lg-4 row">
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Cambiar Contraseña</button>
				</div>
			</div>
		</div>
		</form>
	</div>
<?php
	require("footer.php")
?>
</body>
</html>