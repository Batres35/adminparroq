<?php
function write_log($path, $txt){
	date_default_timezone_set("America/Mexico_City");

	$nombre_archivo = $path."log-".date("m-Y").".txt";
	if($archivo = fopen($nombre_archivo, "a"))
	{
		fwrite($archivo, date("[d-m-Y  H:i:s]")."	".$txt." \n");
    	fclose($archivo);
	}
}
?>