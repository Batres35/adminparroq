<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
	<title>AdminParroq - Alta de Persona</title>
	<meta charset="utf8"/>
    <!-- Bootstrap JS and CSS + jQuery -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
	<div class="panel panel-default">
<!--==================== MENU PRINCIPAL SIN VERIFICACION DE INICIO DE SESION ====================-->
<?php
require_once("menu.php");
show_menu("parroquia","")
?>
<!--==================== MENU PRINCIPAL SIN VERIFICACION DE INICIO DE SESION ====================-->
		<div class="panel-body">
<?php
	$data = htmlspecialchars("&<>''");
	echo $data;
?>
		</div>
	</div>
</body>

</html>