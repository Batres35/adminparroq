<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Lista de Principales");
?>
    <script>
	$(document).ready(function() 
	    { 
	        $("#tabla_principal").tablesorter(); 
	    } 
	); 
	</script>

</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require('conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("parroquia","lista_principal");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
			</div>
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Lista de Principales</h1></div>
			<div class="container">
				<table class="table tablesorter" id="tabla_principal">
				    <thead>
					      <tr>
						    	<th>Nombre</th>
						    	<th>Opciones</th>
					      </tr>
				    </thead>
				    <tbody>
<?php
	$sql = "SELECT * FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);
	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			echo "<tr>";
			echo "<td>".$row['nombre']." ".$row['apellido_paterno']." ".$row['apellido_materno']."</td>";
			echo "<td><div class='btn-group'><a type='button' class='btn btn-primary' href='ver_principal.php?id_principal=".$row['id_principal']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_principal.php?id_principal=".$row['id_principal']."'>Modificar</a>";
			}
			echo "</div></td>";
			echo "</tr>";
		}
	}
?>
				      
				    </tbody>
			  	</table>
			</div>
		</div>
	</div>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>