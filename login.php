<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Login");
?>
</head>
<body>
<?php
	$usuario = null;
	if(isset($_GET['usuario']))
	{
		$usuario = $_GET['usuario'];
	}
?>
<?php
	if(isset($_GET['fail']) && $_GET['fail'] = 'true')
	{
		echo "		<div class='alert alert-danger'>";
		echo "				<p><strong>Usuario o Contraseña Incorrecto</strong><p>";
		echo "		</div>";
	}
?>
	<div class="panel-group">
		<div class="container">
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
				<h2>Iniciar Sesión</h2>
				<form action="check_login.php" method=post class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-lg-4" for="email">Usuario: </label>
						<div class="col-lg-8">
							<input class="form-control" value="<?php echo $usuario?>" name="usuario" type="text" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4" for="pwd">Contraseña: </label>
						<div class="col-lg-8">
							<input class="form-control" name="contrasena" type="password" required></p>
						</div>
					</div>
					<div>
						<input type="submit" name="Login" value="Login" class="btn btn-primary btn-block">
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
	require("footer.php")
?>
</body>
</html>	