<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Matrimonio");
?>
    <script>
    	function DatePicker(){
    		$('#date_matrimonio_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    		$('#date_registro_civil_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}
    </script>

</head>
<body onload="DatePicker()">
<?php
	require_once('check_loggedin.php');
?>
<?php
	require('conexion.php');
?>
<?php
	$id_matrimonio = $_GET["id_matrimonio"];
	$ministro_nombre;

	require('conexion.php');

	$ministro_nombre = "";
 
	$sql = "SELECT * FROM matrimonio WHERE id_matrimonio = $id_matrimonio AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	if($row['matrimonio_ministro'] != null){
		$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[matrimonio_ministro] AND id_parroquia = $_SESSION[id_parroquia]";

		$result = $conexion->query($sql);
		$row2 = $result->fetch_array(MYSQLI_ASSOC);
		$ministro_nombre = $row2['nombre']." ".$row2['apellido_materno']." ".$row2['apellido_materno'];
	}

	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[matrimonio_parroco] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row3 = $result->fetch_array(MYSQLI_ASSOC);

	$sql = "SELECT templo FROM iglesia WHERE id_iglesia = $row[id_iglesia] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row4 = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['matrimonio_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$matrimonio_fecha = implode("/", $array_date);

	$div_date = explode("-",$row['registro_civil_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$registro_civil_fecha = implode("/", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(1);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Matrimonio</h1></div>
			</div>
			<form action="set_update.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="4">
				<input type="hidden" name="id_matrimonio" value="<?php echo $_GET["id_matrimonio"];?>">
				<div class="container">
					<div class="col-lg-4">
						<div>
							<h4>El</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_nombre'];?>" name="persona1_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_apellido_paterno'];?>" name="persona1_apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_apellido_materno'];?>" name="persona1_apellido_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Origen: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_origen'];?>" name="persona1_origen" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Residencia: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_residencia'];?>" name="persona1_residencia" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_padre_nombre'];?>" name="persona1_padre_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona1_madre_nombre'];?>" name="persona1_madre_nombre" required>
							</div>
						</div>
						<div class="invisible">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<label><input type="radio" name="persona1_genero" value="M" checked>Masculino</label>
								<label><input type="radio" name="persona1_genero" value="F">Femenino</label>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Ella</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_nombre'];?>" name="persona2_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_apellido_paterno'];?>" name="persona2_apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_apellido_materno'];?>" name="persona2_apellido_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Origen: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_origen'];?>" name="persona2_origen" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Residencia: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_residencia'];?>" name="persona2_residencia" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_padre_nombre'];?>" name="persona2_padre_nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['persona2_madre_nombre'];?>" name="persona2_madre_nombre" required>
							</div>
						</div>
						<div class="invisible">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<label><input type="radio" name="persona2_genero" value="M">Masculino</label>
								<label><input type="radio" name="persona2_genero" value="F" checked>Femenino</label>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos del Matrimonio</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4" for="sel1">Iglesia:</label>
							<div class="col-lg-8">
								<select class="form-control" name="id_iglesia" id="sel1">
<?php
	 
	$sql = "SELECT id_iglesia, templo FROM iglesia where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);
	if ($result->num_rows > 0) { 
		while($row5 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row5['id_iglesia'] == $row['id_iglesia']){
			echo "<option value='".$row5['id_iglesia']."' selected>".$row5['templo']."</option>";
			}else{
			echo "<option value='".$row5['id_iglesia']."'>".$row5['templo']."</option>";
			}
		} 
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" value="<?php echo $row['acta'];?>" name="acta" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Libro: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" value="<?php echo $row['libro'];?>" name="libro" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Matrimonio: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_matrimonio_fecha" value="<?php echo $matrimonio_fecha;?>" name="matrimonio_fecha" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Proclama: </label>
							<div class="col-lg-4 radio">
								<label><input type="radio" name="matrimonio_proclama" value="PUBLICADAS" <?php if($row['matrimonio_proclama'] == "PUBLICADAS"){ echo "checked"; }?>>Publicadas</label>
								<label><input type="radio" name="matrimonio_proclama" value="DISPENSADAS" <?php if($row['matrimonio_proclama'] == "DISPENSADAS"){ echo "checked"; }?>>Dispensadas</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Ministro de Matrimonio: </label>
							<div class="col-lg-8">
								<select class="form-control" name="matrimonio_ministro" id="sel1">
									<option value=''>NINGUNO</option>
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row5 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row5['id_principal'] == $row['matrimonio_ministro']){
			echo "<option value='".$row5['id_principal']."' selected>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			} else {
				echo "<option value='".$row5['id_principal']."'>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			}
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Párroco de Matrimonio: </label>
							<div class="col-lg-8">
								<select class="form-control" name="matrimonio_parroco" id="sel1">
<?php
	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal where id_parroquia = '$_SESSION[id_parroquia]';";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row5 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row5['id_principal'] == $row['matrimonio_parroco']){
			echo "<option value='".$row5['id_principal']."' selected>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			} else {
				echo "<option value='".$row5['id_principal']."'>".$row5['nombre']." ".$row5['apellido_paterno']." ".$row5['apellido_materno']."</option>";
			}
		}
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Padrinos y Testigos: </label>
							<div class="col-lg-8">
								<textarea class="form-control uppercase" type="text" name="matrimonio_padrinos_testigos" placeholder="Separados por Comas (NOMBRE 1, NOMBRE 2)" required><?php echo $row['matrimonio_padrinos_testigos'];?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div>
						<h4>Datos de Registro Civil</h4>						
					</div>
					<div class="col-lg-1"></div>
					<div class="col-lg-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Lugar de Registro: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" value="<?php echo $row['registro_civil_lugar'];?>" name="registro_civil_lugar" required>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label class="control-label col-lg-6">Fecha de Registro: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" id="date_registro_civil_fecha" value="<?php echo $registro_civil_fecha;?>" name="registro_civil_fecha" required>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label class="control-label col-lg-6">Acta de Registro: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" value="<?php echo $row['registro_civil_acta'];?>" name="registro_civil_acta" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>