<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Matrimonio");
?>
</head>
<body onload="DatePicker()">
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_matrimonio = $_GET["id_matrimonio"];
	$ministro_nombre;

	require('conexion.php');

	$ministro_nombre = "";
 
	$sql = "SELECT * FROM matrimonio WHERE id_matrimonio = $id_matrimonio AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	if($row['matrimonio_ministro'] != null){
		$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[matrimonio_ministro] AND id_parroquia = $_SESSION[id_parroquia]";

		$result = $conexion->query($sql);
		$row2 = $result->fetch_array(MYSQLI_ASSOC);
		$ministro_nombre = $row2['nombre']." ".$row2['apellido_materno']." ".$row2['apellido_materno'];
	}

	$sql = "SELECT id_principal, nombre, apellido_paterno, apellido_materno FROM principal WHERE id_principal = $row[matrimonio_parroco] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row3 = $result->fetch_array(MYSQLI_ASSOC);

	$sql = "SELECT templo FROM iglesia WHERE id_iglesia = $row[id_iglesia] AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row4 = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['matrimonio_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$matrimonio_fecha = implode("-", $array_date);

	$div_date = explode("-",$row['registro_civil_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$registro_civil_fecha = implode("-", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Matrimonio</h1></div>
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="4">
				<div class="container">
					<div class="col-lg-4">
						<div>
							<h4>El</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_apellido_paterno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Origen: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_origen'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Residencia: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_residencia'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_padre_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona1_madre_nombre'];?></p>
							</div>
						</div>
						<div class="invisible">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<p class="form-control-static"><?php if($row['persona1_genero'] != null) { echo $row['persona1_genero']; } ?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Ella</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_apellido_paterno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Origen: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_origen'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Residencia: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_residencia'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre del Padre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_padre_nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre de la Madre: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['persona2_madre_nombre'];?></p>
							</div>
						</div>
						<div class="invisible">
							<label class="control-label col-lg-4">Genero: </label>
							<div class="col-lg-4 radio">
								<p class="form-control-static"><?php if($row['persona2_genero'] != null) { echo $row['persona2_genero']; } ?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<h4>Datos del Matrimonio</h4>						
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4" for="sel1">Iglesia:</label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row4['templo'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Acta: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['acta'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Libro: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['libro'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Matrimonio: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $matrimonio_fecha;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Proclama: </label>
							<div class="col-lg-4 radio">
								<p class="form-control-static"><?php echo $row['matrimonio_proclama'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Ministro de Matrimonio: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $ministro_nombre;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Párroco de Matrimonio: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row3['nombre']." ".$row3['apellido_materno']." ".$row3['apellido_materno'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Padrinos y Testigos: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['matrimonio_padrinos_testigos'];?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div>
						<h4>Datos de Registro Civil</h4>						
					</div>
					<div class="col-lg-1"></div>
					<div class="col-lg-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Lugar de Registro: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['registro_civil_lugar'];?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label class="control-label col-lg-6">Fecha de Registro: </label>
							<div class="col-lg-6">
								<p class="form-control-static"><?php echo $registro_civil_fecha;?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label class="control-label col-lg-6">Acta de Registro: </label>
							<div class="col-lg-6">
								<p class="form-control-static"><?php echo $row['registro_civil_acta'];?></p>
							</div>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
<?php
	mysqli_close($conexion);
?>
</body>
</html>