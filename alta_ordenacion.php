<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Alta Ordenación");
?>
    <script>
    	function DatePicker(){
    		$('#date_ordenacion_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}
    </script>
</head>
<body onload="DatePicker()">
<?php
	require_once('check_loggedin.php');
?>
<?php
	require_once("menu.php");
	show_menu("alta","alta_ordenacion");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class='alert alert-warning col-lg-8'>
					<strong>Por favor verifica que la información sea correcta antes de continuar.</strong>
					<p>No es posible modificar la información posteriormente.</p>
				</div>
			</div>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Ordenación</h1></div>
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="6">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="apellido_materno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Lugar de Ordenación: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="ordenacion_lugar" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Fecha de Ordenación: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_ordenacion_fecha" name="ordenacion_fecha" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Registrar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>