<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Iglesia");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_iglesia = $_GET["id_iglesia"];

	require('conexion.php');
 
	$sql = "SELECT * FROM iglesia WHERE id_iglesia = $id_iglesia AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Ver Iglesia</h1></div>
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="7">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Templo: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['templo'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Parroquia: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['parroquia'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Arquidiocesis: </label>
							<div class="col-lg-8">
								<p class="form-control-static"><?php echo $row['arquidiocesis'];?></p>
							</div>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>