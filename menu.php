<?php

function show_menu($menu, $submenu){
	echo "	<nav class='navbar navbar-default'>\n";
	echo "		<div class='container-fluid'>\n";
	echo "		    <div class='navbar-header'>\n";
	echo "		    	<a href='index.php'>\n";
	echo "     					<img class='img-responsive' width='225' src='img/logo.png' alt='Logo'>\n";
	echo "     				</a>\n";
	echo "    		</div>\n";
	echo "			<ul class='nav navbar-nav'>\n";
	echo "				<li ";if($menu == "inicio"){echo "class='active'";}echo"><a href='index.php'>Inicio</a></li>\n";
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
	{
		echo "				<li class='dropdown ";if($menu == "alta"){echo "active";}echo"'>\n";
		echo "					<a class='dropdown-toggle'  data-toggle='dropdown' href=''>Dar de Alta<span class='caret'></span></a>\n";
		echo "					<ul class='dropdown-menu'>\n";
		echo "						<li ";if($submenu == "alta_bautismo"){echo "class='active'";}echo"><a href='alta_bautismo.php'>Bautismo</a></li>\n";
		echo "						<li ";if($submenu == "alta_confirmacion"){echo "class='active'";}echo"><a href='alta_confirmacion.php'>Confirmación</a></li>\n";
		echo "						<li ";if($submenu == "alta_eucaristia"){echo "class='active'";}echo"><a href='alta_eucaristia.php'>Eucaristía</a></li>\n";
		echo "						<li ";if($submenu == "alta_matrimonio"){echo "class='active'";}echo"><a href='alta_matrimonio.php'>Matrimonio</a></li>\n";
		echo "						<li ";if($submenu == "alta_ordenacion"){echo "class='active'";}echo"><a href='alta_ordenacion.php'>Ordenación</a></li>\n";
		echo "						<li ";if($submenu == "alta_profesion"){echo "class='active'";}echo"><a href='alta_profesion.php'>Profesión</a></li>\n";
		echo "					</ul>\n";
		echo "				</li>\n";
		echo "				<li class='dropdown ";if($menu == "parroquia"){echo "active";}echo"'>\n";
		echo "					<a class='dropdown-toggle'  data-toggle='dropdown' href=''>Parroquia<span class='caret'></span></a>\n";
		echo "					<ul class='dropdown-menu'>\n";
		echo "						<li ";if($submenu == "alta_iglesia"){echo "class='active'";}echo"><a href='alta_iglesia.php'>Alta de Iglesia</a></li>\n";
		echo "						<li ";if($submenu == "alta_principal"){echo "class='active'";}echo"><a href='alta_principal.php'>Alta de Principal</a></li>\n";
		echo "						<li ";if($submenu == "lista_iglesia"){echo "class='active'";}echo"><a href='lista_iglesia.php'>Lista Iglesias</a></li>\n";
		echo "						<li ";if($submenu == "lista_principal"){echo "class='active'";}echo"><a href='lista_principal.php'>Lista Principales</a></li>\n";
		echo "						<li ";if($submenu == "configuracion_parroquia"){echo "class='active'";}echo"><a href='configuracion_parroquia.php'>Configuración</a></li>\n";
		echo "					</ul>\n";
		echo "				</li>\n";
		if(isset($_SESSION['privilegios']) && $_SESSION['privilegios'] >= '2')
		{
			echo "					<li><a href='admin/index.php'>Administrador</a></li>";
		}
		echo "				<form class='navbar-form navbar-left' action='resultado.php' method='get'>\n";
		echo "	  				<div class='input-group'>\n";
		echo "	    				<input type='text' class='form-control' name='busqueda' placeholder='Buscar'>\n";
		echo "	    				<div class='input-group-btn'>\n";
		echo "	      					<button class='btn btn-default' type='submit'>\n";
		echo "	        					<i class='glyphicon glyphicon-search'></i>\n";
		echo "	      					</button>\n";
		echo "	    				</div>\n";
		echo "	  				</div>\n";
		echo "				</form>\n";
	}
	echo "			</ul>\n";
	echo "			<ul class='nav navbar-nav navbar-right'>\n";
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
		echo "				<li class='dropdown ";if($menu == "cuenta"){echo "active";}echo"'>\n";
		echo "					<a class='dropdown-toggle'  data-toggle='dropdown' href=''>Cuenta<span class='caret'></span></a>\n";
		echo "					<ul class='dropdown-menu'>\n";
		if(isset($_SESSION['nombre']))
		{
			echo "						<li ><a>Bienvenido ".$_SESSION['nombre']."</a></li>\n";
		}
		else
		{
			echo "						<li ><a>Bienvenido ".$_SESSION['username']."</a></li>\n";
		}
		echo "						<li ";if($submenu == "configuracion_cuenta"){echo "class='active'";}echo"><a href='configuracion_cuenta.php'>Configuración</a></li>\n";
		echo "					</ul>\n";
		echo "				</li>\n";
		echo "				<li><a href='logout.php'><span class='glyphicon glyphicon-log-out'></span> Cerrar Sesión</a></li>\n";
	} else {
		echo "				<li><a href='login.php'><span class='glyphicon glyphicon-log-in'></span> Iniciar Sesión</a></li>\n";
	}
	echo "			</ul>\n";
	echo "		</div>\n";
	echo "	</nav>\n";
}

?>