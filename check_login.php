<?php
session_start();
?>

<?php
function getRealIP() {
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	return $_SERVER['REMOTE_ADDR'];
}
?>

<?php
require('conexion.php');
require('log/log.php');

$usuario = $_POST['usuario'];
$contrasena = $_POST['contrasena'];
 
$sql = "SELECT id_usuario, usuario.id_parroquia AS id_parroquia, usuario, usuario.nombre AS nombre, apellidos, contrasena, privilegios, vigencia_fecha, habilitado FROM usuario LEFT JOIN parroquia ON usuario.id_parroquia = parroquia.id_parroquia WHERE usuario = '$usuario'";

$result = $conexion->query($sql);

if ($result->num_rows > 0) {     
}
$row = $result->fetch_array(MYSQLI_ASSOC);
if (password_verify($contrasena, $row['contrasena'])) {
	if ($row['habilitado'] == 'NO')
	{
		echo "<script language='javascript'>window.location='alerts/deshabilitada.php'</script>";
		write_log("log/","INTENTO INICIAR SESION EL USUARIO ".$usuario." DE UNA PARROQUIA QUE SE ENCUENTRA DESHABILITADA, DESDE LA IP ".getRealIP().".");
	}
	else
	{
		$_SESSION['loggedin'] = true;
		$_SESSION['username'] = $usuario;
		$_SESSION['start'] = time();
		$_SESSION['expire'] = $_SESSION['start'] + (60 * 60);
		$_SESSION['id_usuario'] = $row['id_usuario'];
		$_SESSION['nombre'] = $row['nombre'];
		$_SESSION['apellidos'] = $row['apellidos'];
		$_SESSION['id_parroquia'] = $row['id_parroquia'];
		$_SESSION['privilegios'] = $row['privilegios'];
		$_SESSION['vigencia_fecha'] = $row['vigencia_fecha'];

		write_log("log/","INICIO SESION EL USUARIO ".$_SESSION['username']." CON ID ".$_SESSION['id_usuario']." DE LA PARROQUIA ".$_SESSION['id_parroquia']." DESDE LA IP ".getRealIP().".");
		echo "<script language='javascript'>window.location='alerts/checklogin.php'</script>";
	} 
} else {
	write_log("log/","INTENTO DE INICIAR SESION CON EL USUARIO ".$usuario." DESDE LA IP ".getRealIP().".");
	echo "<script language='javascript'>window.location='login.php?usuario=".$usuario."&fail=true'</script>";
}
mysqli_close($conexion); 
?>