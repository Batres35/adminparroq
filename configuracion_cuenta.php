<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Configuración de la Cuenta");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require_once("menu.php");
	show_menu("cuenta","");
?>
<?php
	check_loggedin(0);
?>
<?php
	require_once('conexion.php');

	$id_usuario = $_SESSION['id_usuario'];
 
	$sql = "SELECT * FROM usuario WHERE id_usuario = $id_usuario";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Configuracion de la Cuenta</h1></div>
			</div>
			<div class="container">
				<form action="update_usuario.php" method="post" class="form-horizontal">
					<input type="hidden" name="id_usuario" value="<?php echo $id_usuario;?>">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2" for="sel1">Parroquia:</label>
							<div class="col-lg-10">
<?php 
	$sql = "SELECT nombre FROM parroquia WHERE id_parroquia = $row[id_parroquia]";

	$result = $conexion->query($sql);
	$row2 = $result->fetch_array(MYSQLI_ASSOC);

	
?>
								<input class="form-control" type="text" value="<?php echo $row2['nombre']; ?>" maxlength="12" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Usuario: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" value="<?php echo $row['usuario'];?>" maxlength="12" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" name="nombre" value="<?php echo $row['nombre'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Apellidos: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" name="apellidos" value="<?php echo $row['apellidos'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" name="correo_electronico" value="<?php echo $row['correo_electronico'];?>" required>
							</div>
						</div>
					</div>
					<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Guardar Cambios</button>
							<a href="cambiar_contrasena.php" class="btn btn-warning btn-block" role="button">Cambiar Contraseña</a>
						</div>
					</div>
				</div>
				</form>
			</div>
<?php
	require("footer.php")
?>
</body>
</html>