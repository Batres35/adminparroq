<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Añadido Exitosamente");
?>
    <script language="javascript">
    	setTimeout("location.href='../index.php'", 5000);
    </script>
</head>
<body>
	<div class='alert alert-success'>
		<p><strong>Registro añadido exitosamente!</strong></p>
		<p>Redireccionando en 5 segundos... de lo contrario da clic <a href="../index.php">aqui</a></p>
	</div>
</body>