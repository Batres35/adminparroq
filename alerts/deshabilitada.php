<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Cuenta Deshabilitada");
?>
    <script language="javascript">
    	setTimeout("location.href='../index.php'", 5000);
    </script>
</head>
<body>
	<div class='alert alert-danger'>
		<strong>Su cuenta se encuentra deshabilitada</strong><br><br>
		Redireccionando en 5 segundos...
		de lo contrario da clic <a href="../index.php">aqui</a>
	</div>
</body>