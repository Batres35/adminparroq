<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Buscar");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(0);
?>
		<div class="container">
			<div class="col-lg-2"></div>
			<div class="panel-primary col-lg-8">
				<h2>Buscador</h2>
				<form class="form_group form-group-lg" action="resultado" method="get">
	  				<div class="input-group">
	    				<input type="text" class="form-control input-lg" name="busqueda" placeholder="Ingrese un nombre o una acta">
	   					<div class="input-group-btn">
	     						<button class="btn btn-default btn-lg" type="submit">
	       						<i class="glyphicon glyphicon-search"></i>
	     						</button>
    					</div>
  					</div>
				</form>
			</div>
		</div>
<?php
	require("footer.php")
?>
</body>
</html>