<?php
session_start();
?>
<?php
require_once('check_loggedin.php');
require('log/log.php');
check_loggedin(0);
?>
<?php

require('conexion.php');

$tipo = $_POST['tipo'];

if($tipo == 1)
{	
	$log_id = $_POST['id_bautismo'];
	$log_tabla = "BAUTISMO";

	$id_bautismo = htmlspecialchars($_POST['id_bautismo'], ENT_QUOTES, 'UTF-8');
	$acta = htmlspecialchars(mb_strtoupper($_POST['acta']), ENT_QUOTES, 'UTF-8');
	if(isset($_POST['libro']) && $_POST['libro'] != ""){$libro = "'".htmlspecialchars(mb_strtoupper($_POST['libro']), ENT_QUOTES, 'UTF-8')."'";}else{$libro = "null";}
	$id_iglesia = htmlspecialchars($_POST['id_iglesia'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$apellido_materno = htmlspecialchars(mb_strtoupper($_POST['apellido_materno']), ENT_QUOTES, 'UTF-8');
	$nacimiento_fecha = htmlspecialchars($_POST['nacimiento_fecha'], ENT_QUOTES, 'UTF-8');
	$nacimiento_lugar = htmlspecialchars(mb_strtoupper($_POST['nacimiento_lugar']), ENT_QUOTES, 'UTF-8');
	if($_POST['genero'] != ""){$genero = "'".htmlspecialchars($_POST['genero'], ENT_QUOTES, 'UTF-8')."'";}else{$genero = "null";}
	$padre_nombre = htmlspecialchars(mb_strtoupper($_POST['padre_nombre']), ENT_QUOTES, 'UTF-8');
	$madre_nombre = htmlspecialchars(mb_strtoupper($_POST['madre_nombre']), ENT_QUOTES, 'UTF-8');
	$abuelo_paterno = htmlspecialchars(mb_strtoupper($_POST['abuelo_paterno']), ENT_QUOTES, 'UTF-8');
	$abuela_paterna = htmlspecialchars(mb_strtoupper($_POST['abuela_paterna']), ENT_QUOTES, 'UTF-8');
	$abuelo_materno = htmlspecialchars(mb_strtoupper($_POST['abuelo_materno']), ENT_QUOTES, 'UTF-8');
	$abuela_materna = htmlspecialchars(mb_strtoupper($_POST['abuela_materna']), ENT_QUOTES, 'UTF-8');
	$bautismo_fecha = htmlspecialchars($_POST['bautismo_fecha'], ENT_QUOTES, 'UTF-8');
	if($_POST['bautismo_ministro'] != ""){$bautismo_ministro = "'".htmlspecialchars($_POST['bautismo_ministro'], ENT_QUOTES, 'UTF-8')."'";}else{$bautismo_ministro = "null";}
	$bautismo_parroco = htmlspecialchars($_POST['bautismo_parroco'], ENT_QUOTES, 'UTF-8');
	$bautismo_padrino = htmlspecialchars(mb_strtoupper($_POST['bautismo_padrino']), ENT_QUOTES, 'UTF-8');
	$bautismo_madrina = htmlspecialchars(mb_strtoupper($_POST['bautismo_madrina']), ENT_QUOTES, 'UTF-8');
	$registro_civil_lugar = htmlspecialchars(mb_strtoupper($_POST['registro_civil_lugar']), ENT_QUOTES, 'UTF-8');
	$registro_civil_fecha = htmlspecialchars($_POST['registro_civil_fecha'], ENT_QUOTES, 'UTF-8');
	$registro_civil_acta = htmlspecialchars($_POST['registro_civil_acta'], ENT_QUOTES, 'UTF-8');

	if($genero == ""){$genero = null;}

	$div_date = array();

	$div_date = explode("/",$registro_civil_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$registro_civil_fecha = implode("-", $array_date);

	$div_date = explode("/",$nacimiento_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$nacimiento_fecha = implode("-", $array_date);

	$div_date = explode("/",$bautismo_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$bautismo_fecha = implode("-", $array_date);
	 
	$query = "UPDATE bautismo SET
	acta='$acta', libro=$libro, id_iglesia='$id_iglesia', nombre='$nombre', apellido_paterno='$apellido_paterno', apellido_materno='$apellido_materno', nacimiento_fecha='$nacimiento_fecha', nacimiento_lugar='$nacimiento_lugar', genero=$genero, padre_nombre='$padre_nombre', madre_nombre='$madre_nombre', abuelo_paterno='$abuelo_paterno', abuela_paterna='$abuela_paterna', abuelo_materno='$abuelo_materno', abuela_materna='$abuela_materna', bautismo_fecha='$bautismo_fecha', bautismo_ministro=$bautismo_ministro, bautismo_parroco='$bautismo_parroco', bautismo_padrino='$bautismo_padrino', bautismo_madrina='$bautismo_madrina', registro_civil_lugar='$registro_civil_lugar', registro_civil_fecha='$registro_civil_fecha', registro_civil_acta='$registro_civil_acta' 
	WHERE id_bautismo = '$id_bautismo'
	";
}
else if($tipo == 2)
{
	$log_id = $_POST['id_confirmacion'];
	$log_tabla = "CONFIRMACION";

	$id_confirmacion = htmlspecialchars($_POST['id_confirmacion'], ENT_QUOTES, 'UTF-8');
	$acta = htmlspecialchars(mb_strtoupper($_POST['acta']), ENT_QUOTES, 'UTF-8');
	if(isset($_POST['libro']) && $_POST['libro'] != ""){$libro = "'".htmlspecialchars(mb_strtoupper($_POST['libro']), ENT_QUOTES, 'UTF-8')."'";}else{$libro = "null";}
	$id_iglesia = htmlspecialchars($_POST['id_iglesia'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$apellido_materno = htmlspecialchars(mb_strtoupper($_POST['apellido_materno']), ENT_QUOTES, 'UTF-8');
	$nacimiento_fecha = htmlspecialchars($_POST['nacimiento_fecha'], ENT_QUOTES, 'UTF-8');
	$nacimiento_lugar = htmlspecialchars(mb_strtoupper($_POST['nacimiento_lugar']), ENT_QUOTES, 'UTF-8');
	if($_POST['genero'] != ""){$genero = "'".htmlspecialchars($_POST['genero'], ENT_QUOTES, 'UTF-8')."'";}else{$genero = "null";}
	$padre_nombre = htmlspecialchars(mb_strtoupper($_POST['padre_nombre']), ENT_QUOTES, 'UTF-8');
	$madre_nombre = htmlspecialchars(mb_strtoupper($_POST['madre_nombre']), ENT_QUOTES, 'UTF-8');
	$confirmacion_fecha = htmlspecialchars($_POST['confirmacion_fecha'], ENT_QUOTES, 'UTF-8');
	if($_POST['confirmacion_ministro'] != ""){$confirmacion_ministro = "'".htmlspecialchars($_POST['confirmacion_ministro'], ENT_QUOTES, 'UTF-8')."'";}else{$confirmacion_ministro = "null";}
	$confirmacion_parroco = htmlspecialchars($_POST['confirmacion_parroco'], ENT_QUOTES, 'UTF-8');
	if($_POST['confirmacion_padrino'] != ""){$confirmacion_padrino = "'".htmlspecialchars(mb_strtoupper($_POST['confirmacion_padrino']), ENT_QUOTES, 'UTF-8')."'";}else{$confirmacion_padrino = "null";}
	if($_POST['confirmacion_madrina'] != ""){$confirmacion_madrina = "'".htmlspecialchars(mb_strtoupper($_POST['confirmacion_madrina']), ENT_QUOTES, 'UTF-8')."'";}else{$confirmacion_madrina = "null";}
	$bautismo_fecha = htmlspecialchars($_POST['bautismo_fecha'], ENT_QUOTES, 'UTF-8');
	$bautismo_lugar = htmlspecialchars(mb_strtoupper($_POST['bautismo_lugar']), ENT_QUOTES, 'UTF-8');

	$div_date = array();

	$div_date = explode("/",$confirmacion_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$confirmacion_fecha = implode("-", $array_date);

	$div_date = explode("/",$nacimiento_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$nacimiento_fecha = implode("-", $array_date);

	$div_date = explode("/",$bautismo_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$bautismo_fecha = implode("-", $array_date);
	 
	$query = "UPDATE confirmacion SET 
	acta='$acta', libro=$libro, id_iglesia='$id_iglesia', nombre='$nombre', apellido_paterno='$apellido_paterno', apellido_materno='$apellido_materno', nacimiento_fecha='$nacimiento_fecha', nacimiento_lugar='$nacimiento_lugar', genero=$genero, padre_nombre='$padre_nombre', madre_nombre='$madre_nombre', confirmacion_fecha='$confirmacion_fecha', confirmacion_ministro=$confirmacion_ministro, confirmacion_parroco='$confirmacion_parroco', confirmacion_padrino=$confirmacion_padrino, confirmacion_madrina=$confirmacion_madrina, bautismo_fecha='$bautismo_fecha', bautismo_lugar='$bautismo_lugar'
	WHERE id_confirmacion = '$id_confirmacion'
	";
}
else if($tipo == 3)
{
	$log_id = $_POST['id_eucaristia'];
	$log_tabla = "EUCARISTIA";

	$id_eucaristia = htmlspecialchars($_POST['id_eucaristia'], ENT_QUOTES, 'UTF-8');
	$acta = htmlspecialchars(mb_strtoupper($_POST['acta']), ENT_QUOTES, 'UTF-8');
	if(isset($_POST['libro']) && $_POST['libro'] != ""){$libro = "'".htmlspecialchars(mb_strtoupper($_POST['libro']), ENT_QUOTES, 'UTF-8')."'";}else{$libro = "null";}
	$id_iglesia = htmlspecialchars($_POST['id_iglesia'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$apellido_materno = htmlspecialchars(mb_strtoupper($_POST['apellido_materno']), ENT_QUOTES, 'UTF-8');
	if($_POST['genero'] != ""){$genero = "'".htmlspecialchars($_POST['genero'], ENT_QUOTES, 'UTF-8')."'";}else{$genero = "null";}
	$padre_nombre = htmlspecialchars(mb_strtoupper($_POST['padre_nombre']), ENT_QUOTES, 'UTF-8');
	$madre_nombre = htmlspecialchars(mb_strtoupper($_POST['madre_nombre']), ENT_QUOTES, 'UTF-8');
	$eucaristia_fecha = htmlspecialchars($_POST['eucaristia_fecha'], ENT_QUOTES, 'UTF-8');
	if($_POST['eucaristia_ministro'] != ""){$eucaristia_ministro = "'".htmlspecialchars($_POST['eucaristia_ministro'], ENT_QUOTES, 'UTF-8')."'";}else{$eucaristia_ministro = "null";}
	$eucaristia_parroco = htmlspecialchars($_POST['eucaristia_parroco'], ENT_QUOTES, 'UTF-8');
	if($_POST['eucaristia_padrino'] != ""){$eucaristia_padrino = "'".htmlspecialchars(mb_strtoupper($_POST['eucaristia_padrino']), ENT_QUOTES, 'UTF-8')."'";}else{$eucaristia_padrino = "null";}
	if($_POST['eucaristia_madrina'] != ""){$eucaristia_madrina = "'".htmlspecialchars(mb_strtoupper($_POST['eucaristia_madrina']), ENT_QUOTES, 'UTF-8')."'";}else{$eucaristia_madrina = "null";}
	$bautismo_fecha = htmlspecialchars($_POST['bautismo_fecha'], ENT_QUOTES, 'UTF-8');

	$div_date = array();

	$div_date = explode("/",$eucaristia_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$eucaristia_fecha = implode("-", $array_date);

	$div_date = explode("/",$bautismo_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$bautismo_fecha = implode("-", $array_date);
	 
	$query = "UPDATE eucaristia SET
	acta='$acta', libro=$libro, id_iglesia='$id_iglesia', nombre='$nombre', apellido_paterno='$apellido_paterno', apellido_materno='$apellido_materno', genero=$genero, padre_nombre='$padre_nombre', madre_nombre='$madre_nombre', eucaristia_fecha='$eucaristia_fecha', eucaristia_ministro=$eucaristia_ministro, eucaristia_parroco='$eucaristia_parroco', eucaristia_padrino=$eucaristia_padrino, eucaristia_madrina=$eucaristia_madrina, bautismo_fecha='$bautismo_fecha'
	WHERE id_eucaristia = '$id_eucaristia'
	";
}
else if($tipo == 4)
{
	$log_id = $_POST['id_matrimonio'];
	$log_tabla = "MATRIMONIO";

	$id_matrimonio = htmlspecialchars($_POST['id_matrimonio'], ENT_QUOTES, 'UTF-8');
	$acta = htmlspecialchars(mb_strtoupper($_POST['acta']), ENT_QUOTES, 'UTF-8');
	if(isset($_POST['libro']) && $_POST['libro'] != ""){$libro = "'".htmlspecialchars(mb_strtoupper($_POST['libro']), ENT_QUOTES, 'UTF-8')."'";}else{$libro = "null";}
	$id_iglesia = htmlspecialchars($_POST['id_iglesia'], ENT_QUOTES, 'UTF-8');
	$persona1_nombre = htmlspecialchars(mb_strtoupper($_POST['persona1_nombre']), ENT_QUOTES, 'UTF-8');
	$persona1_apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['persona1_apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$persona1_apellido_materno = htmlspecialchars(mb_strtoupper($_POST['persona1_apellido_materno']), ENT_QUOTES, 'UTF-8');
	$persona1_origen = htmlspecialchars(mb_strtoupper($_POST['persona1_origen']), ENT_QUOTES, 'UTF-8');
	$persona1_residencia = htmlspecialchars(mb_strtoupper($_POST['persona1_residencia']), ENT_QUOTES, 'UTF-8');
	$persona1_padre_nombre = htmlspecialchars(mb_strtoupper($_POST['persona1_padre_nombre']), ENT_QUOTES, 'UTF-8');
	$persona1_madre_nombre = htmlspecialchars(mb_strtoupper($_POST['persona1_madre_nombre']), ENT_QUOTES, 'UTF-8');
	if($_POST['persona1_genero'] != ""){$persona1_genero = "'".htmlspecialchars($_POST['persona1_genero'], ENT_QUOTES, 'UTF-8')."'";}else{$persona1_genero = "null";}
	$persona2_nombre = htmlspecialchars(mb_strtoupper($_POST['persona2_nombre']), ENT_QUOTES, 'UTF-8');
	$persona2_apellido_paterno = htmlspecialchars(mb_strtoupper( $_POST['persona2_apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$persona2_apellido_materno = htmlspecialchars(mb_strtoupper($_POST['persona2_apellido_materno']), ENT_QUOTES, 'UTF-8');
	$persona2_origen = htmlspecialchars(mb_strtoupper($_POST['persona2_origen']), ENT_QUOTES, 'UTF-8');
	$persona2_residencia = htmlspecialchars(mb_strtoupper($_POST['persona2_residencia']), ENT_QUOTES, 'UTF-8');
	$persona2_padre_nombre = htmlspecialchars(mb_strtoupper($_POST['persona2_padre_nombre']), ENT_QUOTES, 'UTF-8');
	$persona2_madre_nombre = htmlspecialchars(mb_strtoupper($_POST['persona2_madre_nombre']), ENT_QUOTES, 'UTF-8');
	if($_POST['persona2_genero'] != ""){$persona2_genero = "'".htmlspecialchars($_POST['persona2_genero'], ENT_QUOTES, 'UTF-8')."'";}else{$persona2_genero = "null";}
	$matrimonio_fecha = htmlspecialchars($_POST['matrimonio_fecha'], ENT_QUOTES, 'UTF-8');
	$matrimonio_proclama = htmlspecialchars(mb_strtoupper($_POST['matrimonio_proclama']), ENT_QUOTES, 'UTF-8');
	if($_POST['matrimonio_ministro'] != ""){$matrimonio_ministro = "'".htmlspecialchars($_POST['matrimonio_ministro'], ENT_QUOTES, 'UTF-8')."'";}else{$matrimonio_ministro = "null";}
	$matrimonio_parroco = htmlspecialchars($_POST['matrimonio_parroco'], ENT_QUOTES, 'UTF-8');
	if($_POST['matrimonio_padrinos_testigos'] != ""){$matrimonio_padrinos_testigos = "'".htmlspecialchars(mb_strtoupper($_POST['matrimonio_padrinos_testigos']), ENT_QUOTES, 'UTF-8')."'";}else{$matrimonio_padrinos_testigos = "null";}
	$registro_civil_lugar = htmlspecialchars(mb_strtoupper($_POST['registro_civil_lugar']), ENT_QUOTES, 'UTF-8');
	$registro_civil_fecha = htmlspecialchars($_POST['registro_civil_fecha'], ENT_QUOTES, 'UTF-8');
	$registro_civil_acta =htmlspecialchars( $_POST['registro_civil_acta'], ENT_QUOTES, 'UTF-8');

	$div_date = array();

	$div_date = explode("/",$matrimonio_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$matrimonio_fecha = implode("-", $array_date);

	$div_date = explode("/",$registro_civil_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$registro_civil_fecha = implode("-", $array_date);
	 
	$query = "UPDATE matrimonio SET
	acta='$acta', libro=$libro, id_iglesia='$id_iglesia', persona1_nombre='$persona1_nombre', persona1_apellido_paterno='$persona1_apellido_paterno', persona1_apellido_materno='$persona1_apellido_materno', persona1_origen='$persona1_origen', persona1_residencia='$persona1_residencia', persona1_padre_nombre='$persona1_padre_nombre', persona1_madre_nombre='$persona1_madre_nombre', persona1_genero=$persona1_genero, persona2_nombre='$persona2_nombre', persona2_apellido_paterno='$persona2_apellido_paterno', persona2_apellido_materno='$persona2_apellido_materno', persona2_origen='$persona2_origen', persona2_residencia='$persona2_residencia', persona2_padre_nombre='$persona2_padre_nombre', persona2_madre_nombre='$persona2_madre_nombre', persona2_genero=$persona2_genero, matrimonio_fecha='$matrimonio_fecha', matrimonio_proclama='$matrimonio_proclama', matrimonio_ministro=$matrimonio_ministro, matrimonio_parroco='$matrimonio_parroco', matrimonio_padrinos_testigos=$matrimonio_padrinos_testigos, registro_civil_lugar='$registro_civil_lugar', registro_civil_fecha='$registro_civil_fecha', registro_civil_acta='$registro_civil_acta'
	WHERE id_matrimonio = '$id_matrimonio'
	";
}
else if($tipo == 5)
{
	$log_id = $_POST['id_profesion'];
	$log_tabla = "PROFESION";

	$id_profesion = htmlspecialchars($_POST['id_profesion'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$apellido_materno = htmlspecialchars(mb_strtoupper($_POST['apellido_materno']), ENT_QUOTES, 'UTF-8');
	$profesion_lugar = htmlspecialchars(mb_strtoupper($_POST['profesion_lugar']), ENT_QUOTES, 'UTF-8');
	$profesion_fecha = htmlspecialchars($_POST['profesion_fecha'], ENT_QUOTES, 'UTF-8');

	$div_date = explode("/",$profesion_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$profesion_fecha = implode("-", $array_date);

	$query = "UPDATE profesion SET
	nombre='$nombre', apellido_paterno='$apellido_paterno', apellido_materno='$apellido_materno', profesion_lugar='$profesion_lugar', profesion_fecha='$profesion_fecha'
	WHERE id_profesion = '$id_profesion'
	";
}
else if($tipo == 6)
{
	$log_id = $_POST['id_ordenacion'];
	$log_tabla = "ORDENACION";

	$id_ordenacion = htmlspecialchars($_POST['id_ordenacion'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$apellido_materno = htmlspecialchars(mb_strtoupper($_POST['apellido_materno']), ENT_QUOTES, 'UTF-8');
	$ordenacion_lugar = htmlspecialchars(mb_strtoupper($_POST['ordenacion_lugar']), ENT_QUOTES, 'UTF-8');
	$ordenacion_fecha = htmlspecialchars($_POST['ordenacion_fecha'], ENT_QUOTES, 'UTF-8');

	$div_date = explode("/",$ordenacion_fecha);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$ordenacion_fecha = implode("-", $array_date);

	$query = "UPDATE ordenacion SET
	nombre='$nombre', apellido_paterno='$apellido_paterno', apellido_materno='$apellido_materno', ordenacion_lugar='$ordenacion_lugar', ordenacion_fecha='$ordenacion_fecha'
	WHERE id_ordenacion = '$id_ordenacion'
	";
}
else if($tipo == 7)
{
	$log_id = $_POST['id_iglesia'];
	$log_tabla = "IGLESIA";

	$id_iglesia = htmlspecialchars($_POST['id_iglesia'], ENT_QUOTES, 'UTF-8');
	$templo = htmlspecialchars(mb_strtoupper($_POST['templo']), ENT_QUOTES, 'UTF-8');
	$parroquia = htmlspecialchars(mb_strtoupper($_POST['parroquia']), ENT_QUOTES, 'UTF-8');
	$arquidiocesis = htmlspecialchars(mb_strtoupper($_POST['arquidiocesis']), ENT_QUOTES, 'UTF-8');

	$query = "UPDATE iglesia SET
	templo='$templo', parroquia='$parroquia', arquidiocesis='$arquidiocesis'
	WHERE id_iglesia = '$id_iglesia'
	";
}
else if($tipo == 8)
{
	$log_id = $_POST['id_principal'];
	$log_tabla = "PRINCIPAL";

	$id_principal = htmlspecialchars($_POST['id_principal'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellido_paterno = htmlspecialchars(mb_strtoupper($_POST['apellido_paterno']), ENT_QUOTES, 'UTF-8');
	$apellido_materno = htmlspecialchars(mb_strtoupper($_POST['apellido_materno']), ENT_QUOTES, 'UTF-8');

	$query = "UPDATE principal SET
	nombre='$nombre', apellido_paterno='$apellido_paterno', apellido_materno='$apellido_materno'
	WHERE id_principal = '$id_principal'
	";
}
else if($tipo == 9)
{

}

if ($conexion->query($query) == TRUE) {
	//echo $query;
	write_log("log/","PARROQUIA: ".$_SESSION['id_parroquia']." | EL USUARIO ".$_SESSION['id_usuario']." MODIFICO EL REGISTRO CON EL ID ".$log_id." DE LA TABLA ".$log_tabla.".");
	echo "<script language='javascript'>window.location='alerts/set_update.php'</script>";
}
else 
{
	echo "ERROR AL MODIFICAR REGISTRO, FAVOR DE REPORTARLO CON EL ADMINISTRADOR.";
	write_log("log/","ERROR EN LA CONSULTA: ".$query." | ERROR: ".$conexion->error.".");
}

 mysqli_close($conexion); 
 ?>