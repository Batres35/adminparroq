<?php
	session_start();
	
	
	require('../conexion.php');
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Usuario");
?>
</head>
<body>
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("usuario","alta_usuario");
?>
<?php
	$id_usuario = $_GET["id_usuario"];
 
	$sql = "SELECT * FROM usuario WHERE id_usuario = $id_usuario";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Ver Usuario</h1></div>
			</div>
			<div class="container">
				<form class="form-horizontal"> 
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2" for="sel1">Parroquia:</label>
							<div class="col-lg-10">
								<p class="form-control-static">
<?php
	$sql = "SELECT nombre FROM parroquia WHERE id_parroquia = $row[id_parroquia];";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) {
		$row2 = $result->fetch_array(MYSQLI_ASSOC);
		echo $row2['nombre'];
	} 
?>
								</p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Usuario: </label>
							<div class="col-lg-6">
								<p class="form-control-static"><?php echo $row['usuario'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-6">
								<p class="form-control-static"><?php echo $row['nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Apellidos: </label>
							<div class="col-lg-6">
								<p class="form-control-static"><?php echo $row['apellidos'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-6">
								<p class="form-control-static"><?php echo $row['correo_electronico'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Privilegios: </label>
							<div class="col-lg-2">
								<p class="form-control-static"><?php echo $row['privilegios'];?></p>
							</div>
						</div>
					</div>
					<div class="container">
					<div class="col-lg-4"></div>
				</div>
				</form>
			</div>
</body>
</html>	