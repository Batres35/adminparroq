<?php
session_start();
require_once('../check_loggedin.php');
check_loggedin(3);
?>

<?php
require('../log/log.php');
require_once('../conexion.php');

$id_usuario = $_POST["id_usuario"];
$nueva_contrasena = $_POST['nueva_contrasena'];
$repetir_contrasena = $_POST['repetir_contrasena']; 

if ($nueva_contrasena != $repetir_contrasena)
{
	echo "Las contraseñas no coinciden";
} else {
	if ($nueva_contrasena != "" && $repetir_contrasena != ""){

		$hash = password_hash($nueva_contrasena, PASSWORD_BCRYPT);

		$query = "UPDATE usuario SET 
		contrasena='$hash'
		WHERE 
		id_usuario = $id_usuario
		";

	} else {
		echo "Error al cambiar contraseña.";
	}

	if ($conexion->query($query) === TRUE) {
		echo "<h2>Contraseña Cambiada Exitosamente</h2>";
		write_log("../log/","EL USUARIO ".$_SESSION['username']." CON ID ".$_SESSION['id_usuario']." MODIFICO CONTRASEÑA DEL USUARIO  CON ID ".$id_usuario.".");
		echo "<script language='javascript'>window.location='index.php'</script>";
	} else {
		echo "Error modificar contrasena." . $query . "<br>" . $conexion->error; 
	}
}
mysqli_close($conexion);
?>