<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Ver Parroquia");
?>

</head>
<body onload="DatePicker()">
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
?>
<?php
	require('../conexion.php');
?>
<?php
	$id_parroquia = $_GET["id_parroquia"];
 
	$sql = "SELECT * FROM parroquia WHERE id_parroquia = $id_parroquia";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['vigencia_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$vigencia_fecha = implode("-", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Parroquia</h1></div>
			</div>
			<div class="container">
				<form action="set_parroquia.php" method="post" class="form-horizontal">
				<div class="container">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-10">
								<p class="form-control-static"><?php echo $row['nombre'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Estado: </label>
							<div class="col-lg-10">
								<p class="form-control-static"><?php echo $row['estado'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Municipio: </label>
							<div class="col-lg-10">
								<p class="form-control-static"><?php echo $row['municipio'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Localidad: </label>
							<div class="col-lg-10">
								<p class="form-control-static"><?php echo $row['localidad'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Direccion: </label>
							<div class="col-lg-10">
								<p class="form-control-static"><?php echo $row['direccion'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Telefono: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $row['telefono'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-10">
								<p class="form-control-static"><?php echo $row['correo_electronico'];?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Fecha de Vigencia: </label>
							<div class="col-lg-4">
								<p class="form-control-static"><?php echo $vigencia_fecha;?></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Habilitado: </label>
							<div class="col-lg-2">
								<p class="form-control-static"><?php echo $row['habilitado'];?></p>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
<?php
	mysqli_close($conexion);
?>
</body>
</html>