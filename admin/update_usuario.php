<?php
session_start();
require_once('../check_loggedin.php');
check_loggedin(3);
?>

<?php
require('../log/log.php');
require_once('../conexion.php');

if(isset($_POST['nombre']) && isset($_POST['apellidos']) && isset($_POST['correo_electronico'])) {
	$id_usuario = htmlspecialchars($_POST["id_usuario"], ENT_QUOTES, 'UTF-8');
	$id_parroquia = htmlspecialchars($_POST['id_parroquia'], ENT_QUOTES, 'UTF-8');
	$usuario = htmlspecialchars($_POST['usuario'], ENT_QUOTES, 'UTF-8');
	$privilegios = htmlspecialchars($_POST['privilegios'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$apellidos = htmlspecialchars(mb_strtoupper($_POST['apellidos']), ENT_QUOTES, 'UTF-8');
	$correo_electronico = htmlspecialchars($_POST['correo_electronico'], ENT_QUOTES, 'UTF-8');
}

$query = "UPDATE usuario SET
		id_parroquia='$id_parroquia', usuario='$usuario', privilegios='$privilegios', nombre='$nombre', apellidos='$apellidos', correo_electronico='$correo_electronico'
		WHERE id_usuario=$id_usuario
		";

if ($conexion->query($query) === TRUE) {
	//echo $query;
	$log_id = mysqli_insert_id($conexion);
	write_log("../log/","EL USUARIO ".$_SESSION['username']." CON ID ".$_SESSION['id_usuario']." MODIFICO EL USUARIO ".$usuario." CON ID ".$id_usuario.".");
	echo "<script language='javascript'>window.location='index.php'</script>";
}
else 
{
	echo "ERROR AL MODIFICAR REGISTRO, FAVOR DE REPORTARLO CON EL ADMINISTRADOR.";
	write_log("../log/","ERROR EN LA CONSULTA: ".$query." | ERROR: ".$conexion->error.".");
}
mysqli_close($conexion);
?>