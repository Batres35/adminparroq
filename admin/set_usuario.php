<?php
session_start();
require_once('../check_loggedin.php');
check_loggedin(3);
?>

<?php
require('../log/log.php');
require_once('../conexion.php');

$id_parroquia = htmlspecialchars($_POST['id_parroquia'], ENT_QUOTES, 'UTF-8');
$usuario = htmlspecialchars($_POST['usuario'], ENT_QUOTES, 'UTF-8');
$contrasena = htmlspecialchars($_POST['contrasena'], ENT_QUOTES, 'UTF-8');
$privilegios = htmlspecialchars($_POST['privilegios'], ENT_QUOTES, 'UTF-8');
$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
$apellidos = htmlspecialchars(mb_strtoupper($_POST['apellidos']), ENT_QUOTES, 'UTF-8');
$correo_electronico = htmlspecialchars($_POST['correo_electronico'], ENT_QUOTES, 'UTF-8');

$hash = password_hash($contrasena, PASSWORD_BCRYPT); 

$buscar = "SELECT * FROM usuario WHERE usuario = '$usuario'";
$result = $conexion->query($buscar);
$count = mysqli_num_rows($result);

if ($count == 1) {
	echo "<br />". "El Nombre de Usuario ya a sido tomado." . "<br />";
	echo "<a href='registro.php'>Por favor escoga otro Nombre</a>";
} else {
	if ($usuario != "" && $contrasena != ""){

		$query = "INSERT INTO usuario 
		(id_parroquia, usuario, contrasena, privilegios, nombre, apellidos, correo_electronico) 
		VALUES 
		('$id_parroquia', '$usuario', '$hash', '$privilegios', '$nombre', '$apellidos', '$correo_electronico')";

	} else {
		echo "Error al registrar usuario.";
	}

	if ($conexion->query($query) === TRUE) {
		echo "Usuario Creado Exitosamente.";
		write_log("../log/","EL USUARIO ".$_SESSION['username']." CON ID ".$_SESSION['id_usuario']." REGISTRO USUARIO ".$usuario." EN LA PARROQUIA ".$id_parroquia.".");
		echo "<script language='javascript'>window.location='index.php'</script>";
	} else {
		echo "Error al crear el usuario." . $query . "<br>" . $conexion->error; 
	}
}
mysqli_close($conexion);
?>