<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Parroquia");
?>

    <script>
		function DatePicker(){
    		$('#date_vigencia_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}; 
	</script>

</head>
<body onload="DatePicker()">
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
?>
<?php
	require('../conexion.php');
?>
<?php
	$id_parroquia = $_GET["id_parroquia"];
 
	$sql = "SELECT * FROM parroquia WHERE id_parroquia = $id_parroquia";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);

	$div_date = array();

	$div_date = explode("-",$row['vigencia_fecha']);
	$array_date = array($div_date[2], $div_date[1], $div_date[0]);
	$vigencia_fecha = implode("/", $array_date);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Parroquia</h1></div>
			</div>
			<div class="container">
				<form action="update_parroquia.php" method="post" class="form-horizontal">
				<input type="hidden" name="id_parroquia" value="<?php echo $_GET["id_parroquia"];?>">
				<div class="container">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="nombre" value="<?php echo $row['nombre'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Estado: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="estado" value="<?php echo $row['estado'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Municipio: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="municipio" value="<?php echo $row['municipio'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Localidad: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="localidad" value="<?php echo $row['localidad'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Direccion: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="direccion" value="<?php echo $row['direccion'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Telefono: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" name="telefono" value="<?php echo $row['telefono'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-10">
								<input class="form-control" type="text" name="correo_electronico" value="<?php echo $row['correo_electronico'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Fecha de Vigencia: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_vigencia_fecha" value="<?php echo $vigencia_fecha;?>" name="vigencia_fecha" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Habilitado: </label>
							<div class="col-lg-2">
								<label class="checkbox-inline">
									<input type="checkbox" value="SI" name="habilitado"<?php if($row['habilitado'] == "SI"){echo "checked";}?>>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
						</div>
					</div>
				</div>
				</form>
			</div>
<?php
	mysqli_close($conexion);
?>
</body>
</html>