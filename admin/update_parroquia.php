<?php
session_start();
?>
<?php
require_once('../check_loggedin.php');
require('../log/log.php');
check_loggedin(3);
?>
<?php
require('../conexion.php');

$habilitado = "NO";

if(isset($_POST['nombre']) && $_POST['estado'] && $_POST['municipio'] && $_POST['localidad'] && $_POST['direccion'] && $_POST['telefono'] && $_POST['correo_electronico'])
{
	$id_parroquia = htmlspecialchars($_POST["id_parroquia"], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']), ENT_QUOTES, 'UTF-8');
	$estado = htmlspecialchars(mb_strtoupper($_POST['estado']), ENT_QUOTES, 'UTF-8');
	$municipio = htmlspecialchars(mb_strtoupper($_POST['municipio']), ENT_QUOTES, 'UTF-8');
	$localidad = htmlspecialchars(mb_strtoupper($_POST['localidad']), ENT_QUOTES, 'UTF-8');
	$direccion = htmlspecialchars(mb_strtoupper($_POST['direccion']), ENT_QUOTES, 'UTF-8');
	$telefono = htmlspecialchars(mb_strtoupper($_POST['telefono']), ENT_QUOTES, 'UTF-8');
	$correo_electronico = htmlspecialchars($_POST['correo_electronico'], ENT_QUOTES, 'UTF-8');
	$vigencia_fecha = htmlspecialchars($_POST['vigencia_fecha'], ENT_QUOTES, 'UTF-8');
	if(isset($_POST['habilitado']) && $_POST['habilitado'] == "SI"){$habilitado = "SI";}
}

$div_date = array();

$div_date = explode("/",$vigencia_fecha);
$array_date = array($div_date[2], $div_date[1], $div_date[0]);
$vigencia_fecha = implode("-", $array_date);
 
$query = "UPDATE parroquia SET
nombre='$nombre', estado='$estado', municipio='$municipio', localidad='$localidad', direccion='$direccion', telefono='$telefono', correo_electronico='$correo_electronico', vigencia_fecha='$vigencia_fecha', habilitado='$habilitado'
WHERE id_parroquia = '$id_parroquia'
";

if ($conexion->query($query) === TRUE) {
	//echo $query;
	$log_id = mysqli_insert_id($conexion);
	write_log("../log/","EL USUARIO ".$_SESSION['username']." CON ID ".$_SESSION['id_usuario']." MODIFICO LA PARROQUIA CON ID ".$id_parroquia.".");
	echo "<script language='javascript'>window.location='index.php'</script>";
}
else 
{
	echo "ERROR AL MODIFICAR REGISTRO, FAVOR DE REPORTARLO CON EL ADMINISTRADOR.";
	write_log("../log/","ERROR EN LA CONSULTA: ".$query." | ERROR: ".$conexion->error.".");
}

 mysqli_close($conexion); 
 ?>