<?php

function show_head($title)
{
	echo "	<meta charset='utf8'/>\n";
	echo "	<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n";
	echo "	<meta name='description' content='Sistema de Gestión Parroquial'>\n";
	echo "	<link rel='shortcut icon' type='image/x-icon' href='../img/favicon.png'>\n";
	echo "	<link rel='stylesheet' href='../css/style.css'>\n";
	echo "	<title>AdminParroq - ".$title."</title>\n";
	//JQuery
	echo "	<script src='../js/jquery-3.2.1.min.js'></script>\n";
	echo "	<script src='../js/bootstrap.min.js'></script>\n";
	//Bootstrap 3
	echo "	<link rel='stylesheet' href='../css/bootstrap.min.css'>\n";
	echo "	<link id='bsdp-css' rel='stylesheet' href='../css/bootstrap-datepicker3.min.css'>\n";
    echo "	<script src='../js/bootstrap-datepicker.min.js'></script>\n";
    //Bootstrap Datepicker
    echo "	<script src='../js/locales/bootstrap-datepicker.es.min.js'></script>\n";
    //TableSorter
    echo " <script type='text/javascript' src='../js/jquery.tablesorter.js'></script>\n";
}
?>