<?php
	session_start();
	
	
	require('../conexion.php');
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Usuario");
?>
</head>
<body>
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("usuario","alta_usuario");
?>
<?php
	$id_usuario = $_GET["id_usuario"];
 
	$sql = "SELECT * FROM usuario WHERE id_usuario = $id_usuario";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Usuario</h1></div>
			</div>
			<div class="container">
				<form action="update_usuario.php" method="post" class="form-horizontal">
					<input type="hidden" name="id_usuario" value="<?php echo $_GET["id_usuario"];?>">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2" for="sel1">Parroquia:</label>
							<div class="col-lg-10">
								<select class="form-control" name="id_parroquia" id="sel1">
								<option value='0'>Selecciona una Parroquia</option>
<?php
	$sql = "SELECT id_parroquia, nombre FROM parroquia;";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row2 = $result->fetch_array(MYSQLI_ASSOC)){
			if ($row2['id_parroquia'] == $row['id_parroquia']){
			echo "<option value='".$row2['id_parroquia']."' selected>".$row2['nombre']."</option>";
			}else{
			echo "<option value='".$row2['id_parroquia']."'>".$row2['nombre']."</option>";
			}
		} 
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Usuario: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" name="usuario" value="<?php echo $row['usuario'];?>" maxlength="12" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" name="nombre" value="<?php echo $row['nombre'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Apellidos: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" name="apellidos" value="<?php echo $row['apellidos'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" name="correo_electronico" value="<?php echo $row['correo_electronico'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Privilegios: </label>
							<div class="col-lg-2">
								<input type="number" name="privilegios" class="form-control" min="0" max="3" value="<?php echo $row['privilegios'];?>" required>
							</div>
						</div>
					</div>
					<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
							<a href="cambiar_contrasena?id_usuario=<?php echo $row['id_usuario'];?>" class="btn btn-primary btn-block" role="button">Cambiar Contraseña</a>
						</div>
					</div>
				</div>
				</form>
			</div>
</body>
</html>	