<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Alta Parroquia");
?>

    <script>
		function DatePicker(){
    		$('#date_vigencia_fecha').datepicker({
    			format: "dd/mm/yyyy",
    			language: "es"
    		});
    	}; 
	</script>

</head>
<body onload="DatePicker()">
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
?>
<?php
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("parroquia","alta_parroquia");
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Parroquia</h1></div>
			</div>
			<div class="container">
				<form action="set_parroquia.php" method="post" class="form-horizontal">
				<div class="container">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Estado: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="estado" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Municipio: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="municipio" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Localidad: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="localidad" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Direccion: </label>
							<div class="col-lg-10">
								<input class="form-control uppercase" type="text" name="direccion" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Telefono: </label>
							<div class="col-lg-4">
								<input class="form-control uppercase" type="text" name="telefono" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-10">
								<input class="form-control" type="text" name="correo_electronico" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Fecha de Vigencia: </label>
							<div class="col-lg-4">
								<input class="form-control" type="text" id="date_vigencia_fecha" name="vigencia_fecha" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Habilitado: </label>
							<div class="col-lg-2">
								<label class="checkbox-inline">
									<input type="checkbox" value="SI" name="habilitado">
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Registrar</button>
						</div>
					</div>
				</div>
				</form>
			</div>
<?php
	mysqli_close($conexion);
?>
</body>
</html>