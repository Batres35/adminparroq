<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Administrador");
?>

    <script>
	$(document).ready(function() 
	    { 
	        $("#tabla_parroquia").tablesorter(); 
	    } 
	); 
	</script>

</head>
<body>
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
?>
<?php
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("inicio","");
?>
	<div class="container">
	</div>
<?php
	mysqli_close($conexion);
?>
</body>
</html>