<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Lista Parroquia");
?>

    <script>
	$(document).ready(function() 
	    { 
	        $("#tabla_parroquia").tablesorter(); 
	    } 
	); 
	</script>

</head>
<body>
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
?>
<?php
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("parroquia","lista_parroquia");
?>
			<div class="container">
				<table class="table tablesorter" id="tabla_parroquia">
				    <thead>
				      	<tr>
					        <th>Parroquia</th>
					        <th>Vigencia</th>
					        <th>Habilitado</th>
					        <th>Opciones</th>
				      	</tr>
				    </thead>
				    <tbody>
<?php
	$sql = "SELECT id_parroquia, nombre, vigencia_fecha, habilitado FROM parroquia;";
	$div_date = array();
	
	$result = $conexion->query($sql);
	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			
			$div_date = explode("-",$row['vigencia_fecha']);
			$array_date = array($div_date[2], $div_date[1], $div_date[0]);
			$vigencia_fecha = implode("/", $array_date);

			echo "<tr>";
			echo "<td>".$row['nombre']."</td>";
			if (comprobar_vigencia($row['vigencia_fecha']) < 0)
			{
				echo "<td><strong class='text-danger'>".$vigencia_fecha."</strong></td>";
			}
			else
			{
				echo "<td>".$vigencia_fecha."</td>";
			}
			echo "<td>".$row['habilitado']."</td>";
			echo "<td><div class='btn-group'><a type='button' class='btn btn-primary' href='ver_parroquia.php?id_parroquia=".$row['id_parroquia']."'>Ver</a>";
			if(1 <= $_SESSION['privilegios']){
				echo "<a type='button' class='btn btn-primary' href='modificar_parroquia.php?id_parroquia=".$row['id_parroquia']."'>Modificar</a>";
			}
			echo "</div></td>";
			echo "</tr>";
		}
	}
?>
					</tbody>
			  	</table>
			</div>
<?php
	mysqli_close($conexion);
?>
</body>
</html>