<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Lista Usuarios");
?>

    <script>
	$(document).ready(function() 
	    { 
	        $("#tabla_usuario").tablesorter(); 
	    } 
	); 
	</script>

</head>
<body>
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
?>
<?php
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("usuario","lista_usuario");
?>
			<div class="container">
				<table class="table tablesorter" id="tabla_usuario">
				    <thead>
				      	<tr>
					        <th>Usuario</th>
					        <th>Nombre</th>
					        <th>Parroquia</th>
					        <th>Tipo</th>
					        <th>Opciones</th>
				      	</tr>
				    </thead>
				    <tbody>
<?php
	$sql = "SELECT id_usuario, usuario, usuario.nombre AS nombre, apellidos, parroquia.nombre AS parroquia, privilegios FROM usuario JOIN parroquia WHERE parroquia.id_parroquia = usuario.id_parroquia;";

	$result = $conexion->query($sql);
	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			if($row['privilegios'] == 0){
				$privilegios = "ESTANDAR";
			}else if($row['privilegios'] == 1){
				$privilegios = "ADMIN";
			}else if($row['privilegios'] == 2){
				$privilegios = "SUPERVISOR";
			}else if($row['privilegios'] == 3){
				$privilegios = "ROOT";
			}else{
				$privilegios = "UNKNOWN";
			}

			echo "<tr>";
			echo "<td>".$row['usuario']."</td>";
			echo "<td>".$row['nombre']." ".$row['apellidos']."</td>";
			echo "<td>".$row['parroquia']."</td>";
			echo "<td>".$privilegios."</td>";
			echo "<td><div class='btn-group'>";
			echo "<a type='button' class='btn btn-primary' href='ver_usuario.php?id_usuario=".$row['id_usuario']."'>Ver</a>";
			echo "<a type='button' class='btn btn-primary' href='modificar_usuario.php?id_usuario=".$row['id_usuario']."'>Modificar</a>";
			echo "</div></td>";
			echo "</tr>";
		}
	}
?>
					</tbody>
			  	</table>
			</div>
<?php
	mysqli_close($conexion);
?>
</body>
</html>