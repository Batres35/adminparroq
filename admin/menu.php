<?php

function show_menu($menu, $submenu){
	echo "		<nav class='navbar navbar-inverse'>";
	echo "			<div class='container-fluid'>";
	echo "			    <div class='navbar-header'>";
	echo "			    	<a href='../index.php'>";
	echo "     					<img class='img-responsive' width='225' src='../img/logo.png' alt='Logo'>\n";
	echo "      				</a>";
	echo "    			</div>";
	echo "				<ul class='nav navbar-nav'>";
	echo "					<li ";if($menu == "inicio"){echo "class='active'";}echo"><a href='index.php'>Inicio</a></li>";
	echo "					<li class='dropdown ";if($menu == "parroquia"){echo "active";}echo"'>";
	echo "						<a class='dropdown-toggle'  data-toggle='dropdown' href=''>Parroquia<span class='caret'></span></a>";
	echo "						<ul class='dropdown-menu'>";
	echo "							<li ";if($submenu == "lista_parroquia"){echo "class='active'";}echo"><a href='lista_parroquia.php'>Lista</a></li>";
	echo "							<li ";if($submenu == "alta_parroquia"){echo "class='active'";}echo"><a href='alta_parroquia.php'>Dar de Alta</a></li>";
	echo "						</ul>";
	echo "					</li>";
	echo "					<li class='dropdown ";if($menu == "usuario"){echo "active";}echo"'>";
	echo "						<a class='dropdown-toggle'  data-toggle='dropdown' href=''>Usuarios<span class='caret'></span></a>";
	echo "						<ul class='dropdown-menu'>";
	echo "							<li ";if($submenu == "lista_usuario"){echo "class='active'";}echo"><a href='lista_usuario.php'>Lista</a></li>";
	echo "							<li ";if($submenu == "alta_usuario"){echo "class='active'";}echo"><a href='alta_usuario.php'>Dar de Alta</a></li>";
	echo "						</ul>";
	echo "					</li>";
	echo "				</ul>";
	echo "				<ul class='nav navbar-nav navbar-right'>";
	echo "					<li><a href='../logout.php'><span class='glyphicon glyphicon-log-out'></span> Cerrar Sesión</a></li>";
	echo "				</ul>";
	echo "			</div>";
	echo "		</nav>";
}

?>