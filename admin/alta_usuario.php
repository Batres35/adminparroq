<?php
	session_start();
	
	
	require('../conexion.php');
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Alta Usuario");
?>
</head>
<body>
<?php
	require_once('../check_loggedin.php');
	check_loggedin(3);
	require('../conexion.php');
?>
<?php
	require_once("menu.php");
	show_menu("usuario","alta_usuario");
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Usuario</h1></div>
			</div>
			<div class="container">
				<form action="set_usuario.php" method="post" class="form-horizontal"> 
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-2" for="sel1">Parroquia:</label>
							<div class="col-lg-10">
								<select class="form-control" name="id_parroquia" id="sel1">
								<option value='0'>Selecciona una Parroquia</option>
<?php
	$sql = "SELECT id_parroquia, nombre FROM parroquia;";

	$result = $conexion->query($sql);

	if ($result->num_rows > 0) { 
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			echo "<option value='".$row['id_parroquia']."'>".$row['nombre']."</option>";
		} 
	}
?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Usuario: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" name="usuario" maxlength="12" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Nombre: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Apellidos: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="text" name="apellidos" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Correo Electronico: </label>
							<div class="col-lg-6">
								<input class="form-control" type="text" name="correo_electronico" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Contraseña: </label>
							<div class="col-lg-6">
								<input class="form-control uppercase" type="password" name="contrasena" maxlength="12" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-2">Privilegios: </label>
							<div class="col-lg-2">
								<input type="number" name="privilegios" class="form-control" min="0" max="3" value="0" required>
							</div>
						</div>
					</div>
					<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Registrar</button>
						</div>
					</div>
				</div>
				</form>
			</div>
</body>
</html>	