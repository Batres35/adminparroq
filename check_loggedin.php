<?php
	function check_loggedin($privilegios){
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
			if($privilegios <= $_SESSION['privilegios']){
				if(isset($_SESSION['vigencia_fecha']) && comprobar_vigencia($_SESSION['vigencia_fecha']) < 0)
				{
					no_vigente();
				}
			} else {
				access_denied();
			}
		} else {
	   		loggedin_false();
		}

		$now = time();

		if($now > $_SESSION['expire']) {
			loggedin_end();
		}
	}

	function comprobar_vigencia($vigencia){
		$vigencia_valores = explode ("-", $vigencia);
		$vigencia_fecha = gregoriantojd($vigencia_valores[1], $vigencia_valores[2], $vigencia_valores[0]);
		$hoy = gregoriantojd(date("m"), date("d"), date("Y"));     

		return  $vigencia_fecha - $hoy;
	}

	function no_vigente(){
		echo "<div class='container'>\n";
		echo "<div class='alert alert-danger'>\n";
	   	echo "<p><strong>Su cuenta esta vencida!</strong><p>\n";
	   	echo "<p>Pague dentro del plazo establecido, de lo contrario su cuenta sera deshabilitada.<p>\n";
	   	echo "</div>\n";
	   	echo "</div>\n";
	}

	function loggedin_false(){
		echo "<div class='alert alert-warning'>";
	   	echo "Nacesitas <a href='login.php'>Iniciar Sesion</a> para continuar.";
	   	echo "</div>";
		exit;
	}

	function access_denied(){
		echo "<div class='alert alert-warning'>";
	   	echo "<p>No tienes los suficientes privilegios para acceder a esta pagina.</p>";
	   	echo "<p>Consultalo con tu administrador.</p>";
	   	echo "</div>";
		exit;
	}

	function loggedin_end(){
		session_destroy();
		echo "<div class='alert alert-warning'>";
	   	echo "Su sesion ha terminado, necesitas <a href='login.php'>Iniciar Sesion</a> para continuar.";
	   	echo "</div>";
		exit;
	}
	
?>