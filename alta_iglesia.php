<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Alta Iglesia");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	require_once("menu.php");
	show_menu("parroquia","alta_iglesia");
?>
<?php
	check_loggedin(0);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class='alert alert-warning col-lg-8'>
					<strong>Por favor verifica que la información sea correcta antes de continuar.</strong>
					<p>No es posible modificar la información posteriormente.</p>
				</div>
			</div>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Dar de Alta Iglesia</h1></div>
			</div>
			<form action="set_alta.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="7">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Templo: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="templo" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Parroquia: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="parroquia" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Arquidiocesis: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="arquidiocesis" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Registrar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>