<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Iglesia");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_iglesia = $_GET["id_iglesia"];

	require('conexion.php');
 
	$sql = "SELECT * FROM iglesia WHERE id_iglesia = $id_iglesia AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(1);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class='alert alert-danger col-lg-8'>
					<strong>Cualquier cambio en la informacion quedara registrado por el sistema.</strong>
					<p>Por favor verifica la informacion.</p>
				</div>
			</div>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Iglesia</h1></div>
			</div>
			<form action="set_update.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="7">
				<input type="hidden" name="id_iglesia" value="<?php echo $_GET["id_iglesia"];?>">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Templo: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="templo" value="<?php echo $row['templo'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Parroquia: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="parroquia" value="<?php echo $row['parroquia'];?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Arquidiocesis: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" type="text" name="arquidiocesis" value="<?php echo $row['arquidiocesis'];?>" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>