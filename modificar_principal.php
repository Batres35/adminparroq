<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="es">

<head>
<?php
	require_once("head.php");
	show_head("Modificar Principal");
?>
</head>
<body>
<?php
	require_once('check_loggedin.php');
?>
<?php
	$id_principal = $_GET["id_principal"];

	require('conexion.php');
 
	$sql = "SELECT * FROM principal WHERE id_principal = $id_principal AND id_parroquia = $_SESSION[id_parroquia]";

	$result = $conexion->query($sql);
	$row = $result->fetch_array(MYSQLI_ASSOC);
?>
<?php
	require_once("menu.php");
	show_menu("","");
?>
<?php
	check_loggedin(1);
?>
			<div class="container">
				<div class="col-lg-2"></div>
				<div class="col-lg-8"><h1>Modificar Principal</h1></div>
			</div>
			<form action="set_update.php" method="post" class="form-horizontal">
				<input type="hidden" name="tipo" value="8">
				<input type="hidden" name="id_principal" value="<?php echo $_GET["id_principal"];?>">
				<div class="container">
					<div class="col-lg-1"></div>
					<div class="col-lg-8">
						<div class="form-group row">
							<label class="control-label col-lg-4">Nombre: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" value="<?php echo $row['nombre'];?>" type="text" name="nombre" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Paterno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" value="<?php echo $row['apellido_paterno'];?>" type="text" name="apellido_paterno" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-lg-4">Apellido Materno: </label>
							<div class="col-lg-8">
								<input class="form-control uppercase" value="<?php echo $row['apellido_materno'];?>" type="text" name="apellido_materno" required>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 row">
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Modificar</button>
						</div>
					</div>
				</div>
			</form>
<?php
	require("footer.php")
?>
</body>
</html>